/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home-admin page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/': { view: 'pages/homepage' },

  //Terapeuta


  'POST/terapeuta/': 'TerapeutaController.create',
  'GET/terapeuta': 'TerapeutaController.findAll',
  'GET/terapeuta/:id': 'TerapeutaController.findOne',
  'DELETE/terapeuta/:id': 'TerapeutaController.delete',
  'PUT/terapeuta/:id': 'TerapeutaController.update',
  'PUT /login/': {controller: 'TerapeutaController', action:'login'},
  'PUT /reset/': {controller: 'TerapeutaController', action:'reset'},

  //Secuencia

  'POST /secuencia' : 'SecuenciaController.create',
  'PUT /secuencia' : 'SecuenciaController.update',
  'GET /secuencia/obtener' : 'SecuenciaController.obtener',
  'PUT /secuencia/buscar' : 'SecuenciaController.buscar',

  //Multimedia
  'POST /imagen/upload':{controller: 'ImagenController', action:'upload'},
  'POST /imagen/uploadfile':{controller: 'ImagenController', action:'uploadFile'},
  'POST /sonido/upload' : {controller: 'SonidoController', action:'upload'},


  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/


};
