/**
 * Terapeuta.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    nombre:{
      type: 'string',
      required: true
    },
    apellido:{
      type: 'string',
      required: true
    },
    nombreUsuario:{
      type: 'string',
      unique:true,
      required: true
    },
    contrasena:{
      type: 'string',
      required: true,
      //encrypt: true
    },

  },
};

