/**
 * Secuencia.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {


    identificador:{
      type: 'number',
      autoIncrement: true,
      unique: true
    },
    tipo:{
      type:'string',
      required: true
    },
    nombre:{
      type:'string',
      unique: true,
      required: true
    },
    estado:{
      type:'string',
      required: true
    },

    //Asociaciones

    imagenes:{
      collection: 'Imagen',
      via: 'imagen'
    },

    sonidos:{
      collection: 'Sonido',
      via:'sonido'
    },




  },


};

