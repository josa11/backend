/**
 * SecuenciaController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

  create: function (req, res) {

    let   identificador = req.param('identificador'),
          tipo = req.param('tipo'),
          nombre = req.param('nombre'),
          estado = req.param('estado')

    if(!tipo){ return res.badRequest({err:'Tipo invalido'}) }
    if (!nombre){ return res.badRequest({err:'Nombre invalido'})}
    if (!estado){ return res.badRequest({err:'Estado invalido'})}
    if (!estado){return res.badRequest({err:'Estado invalido'}) }



    Secuencia.create({
      identificador: identificador,
      tipo: tipo,
      nombre: nombre,
      estado: estado,

    }).fetch().then(Secuencia =>{
       return res.json({'id': Secuencia.id,'tipo': Secuencia.tipo, 'identificador': Secuencia.identificador});
    })
      .catch(err => res.serverError(err.message));
  },

  obtener:function (req, res) {

    Secuencia.find({}).sort([{identificador: 'DESC'}]).limit(1).then(identificador=>{
      if (!identificador ) return res.notFound({err: 'Ultimo no enontrado'})

      return res.ok(identificador);
    })
      .catch(err => res.serverError(err));
    },

  update:function (req, res) {

    let identificador = req.param('identificador'),
        tipo = req.param('tipo'),
        nombre = req.param('nombre'),
        estado = req.param('estado'),
        secuenciaId = req.param('id');

    if(!secuenciaId) return res.badRequest({err: 'Falta el id de la secuencia'})

    let secuencia={};
    if(identificador){
      secuencia.identificador= identificador;
    }
    if(tipo){
      secuencia.tipo= tipo;
    }
    if(nombre){
      secuencia.nombre= nombre;
    }
    if(estado){
      secuencia.estado =estado;
    }
    Secuencia.update({id: secuenciaId }, secuencia)
      .then(secuencia =>{
        return res.ok({
            identificador: identificador,
            nombre: nombre,
            tipo: tipo,
            estado: estado
          }
        );
      }).catch(err => res.serverError(err));
  },

  buscar: function(req, res) {

    let nombre = req.body.nombre

    Secuencia.findOne({nombre:nombre}).then(_secuencia=>{
      if(!_secuencia){return res.json({err:'Nombre no encontrado'});}
      else if(_secuencia.nombre===nombre){return res.json({err:'Nombre ya existe'});}
    })
  },


};


