
var async = require("async");

module.exports = {

  uploadFile: function (req, res) {
    req.file('file').upload({
        //dirname : process.cwd() + '../../assets/images/uploads/',
        dirname: '../../assets/images',
        maxBytes: 10000000
      },
      function (err, file) {
        if (err) console.error("ERROR" + err);
        res.json({"status": "Arcvhivo cargado", "file": file})
        console.log(file)
        Imagen.create({
          ruta: file.fd,
          tipo: file.type,
          nombre: file.filename
        }).then(Imagen => {
          return res.ok();
        }).catch(err => res.serverError(err.message));

      });


  },

  hello: function (req, res) {

  },

  upload :function  (req, res) {

    var imagen=  req.param('imagen');
    var tipo = req.param('tipo');
    var identificador= req.param('identificador');

    console.log(identificador);


    if(req.method === 'GET')

      return res.json({'status':'GET no permitido'});

    // Call to /upload via GET is error
    var upFile = req.file('upFile');


    if(tipo==='Asociación Imagen-Sonido') {
      var path = '../../assets/Imagenes/asociacion/'+ identificador + '/';
    }else {
      var path = '../../assets/Imagenes/secuencia/'+identificador+'/';
    }
    var mkdirp = require('mkdirp');

    mkdirp(path, function(err) {
      if (err) {
        console.log("folder not created: ", err);
        return res.serverError(err);
      }
      console.log("folder created or existed!")

      upFile.upload({maxBytes: 1000000000, dirname: path,  adapter: require('skipper-disk') },function onUploadComplete (err, files) {
        if (err) {
          console.log(" file not uploaded. Error: "+err);
          return res.serverError(err);
        }
        else {
          //for (let u in files) {



            var filePath = files[0].fd;
            var fileSize = files[0].size;
            var fileType = files[0].type;
            var fileName = files[0].filename;


            Imagen.create({
              name: fileName,
              version: 0,
              description: fileType,
              file: fileName,
              fileSize: fileSize,
              fullPath: filePath,
              imagen: imagen,
            }, function Done(err) {
              return res.ok();

            });
          }
        //}
      });
    });


  },



  buscar: function(req, res) {
    Secuencia.findOne(req.param('id'))
      .populate('imagenes')
      .exec(function(err,user) {
        //console.log(found);
        res.view({
          imagenes: secuencia.imagenes,
          secuencia: secuencia
        });
      });
  },

  asociarImagen:function(req, res, next){
    Imagen.find(function encontrarImagenes(err, imagenes) {
      if(err) return next(err);

      res.view({
        imagenes: imagenes,
        secuencia_id: req.param('id')
      })
    })
  },

  desasociarImagen:function(req, res, next){
    var imagen_id= req.param('imagen_id');
    var secuencia_id= req.param('secuencia_id');

    Secuencia.findOne(secuencia_id, function encontrarImagen(err, user) {
      if (err) return next(err);
      if (!Secuencia) return next();

      secuencia.imagenes.remove(imagen_id);

      secuencia.save(function  createDeassociation(err, saved) {
        if (err) console.log(err);
      })
    })
  },

  asociarImagenSecuencia: function (req,res,next) {
    var imagen_id= req.param('imagen_id');
    var secuencia_id= req.param('secuencia_id');

    Secuencia.findOne({id:secuencia_id}).then(function (secuencia) {
      secuencia.imagenes.add(imagen_id);
      return secuencia.save().fail(function () {
        sails.log.info('User already has that pet!');
      })
    }).then(function () {
    }).fail(function (err) {
      sails.log.error('Unexpected error: ' +err);
    })
  }


};


