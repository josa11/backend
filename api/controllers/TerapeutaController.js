﻿/**
 * TerapeutaController
 *
 *
@description :: Server-side actions for handling incoming requests.

* @help        :: See https://sailsjs.com/docs/concepts/actions


*/


module.exports = {

  

create: function (req, res) {

    
let     nombre = req.param('nombre'),
        apellido = req.param('apellido'),
        nombreUsuario = req.param('nombreUsuario'),
        contrasena = req.param('contrasena')

if(!nombre){
      return res.badRequest({err:'Nombre invalido'})
    }
    
if (!apellido){
      return res.badRequest({err:'Apellido invalido'})
    }
    
if (!nombreUsuario){
      return res.badRequest({err:'Nombre de Usuario invalido'})
    }
if (!contrasena){
      return res.badRequest({err:'Contraseña invalida'})
}

Terapeuta.create({

  nombre: nombre,
  apellido: apellido,
  nombreUsuario: nombreUsuario,
  contrasena: contrasena,
    }).then(Terapeuta =>{
        //if(!terapeuta.nombre)
        //return res.serverError({err: 'No se puede crear Terapeuta'});
        //return terapeuta;
        return res.ok({
            nombre: nombre,
            apellido: apellido,
            nombreUsuario: nombreUsuario,
            contrasena: contrasena
        });
      })
      
    .catch(err => res.serverError(err.message));
},

update:function (req, res) {

    let nombre = req.param('nombre'),
        apellido = req.param('apellido'),
        nombreUsuario = req.param('nombreUsuario'),
        contrasena = req.param('contrasena'),
        terapeutaId = req.params.id;

    if(!terapeutaId) return res.badRequest({err: 'Falta el id del terapeuta'})

    let terapeuta={};
    if(nombre){
      terapeuta.nombre= nombre;
    }
    if(apellido){
      terapeuta.apellido= apellido;
    }
    if(nombreUsuario){
      terapeuta.nombreUsuario= nombreUsuario;
    }
    if(contrasena){
      terapeuta.contrasena =contrasena;
    }
    Terapeuta.update({id: terapeutaId }, terapeuta)
      .then(terapeuta =>{
          return res.ok({
            nombre: nombre,
            apellido: apellido,
            nombreUsuario: nombreUsuario,
            contrasena: contrasena
            }
            );
      }).catch(err => res.serverError(err));
  },

  delete:function (req, res) {
    let terapeutaId = req.params.id;
    if(!terapeutaId) return res.badRequest({err: 'Falta el id de Terapeuta'});

    terapeuta.destroy({id: terapeutaId})
      .then(_terapeuta =>{
        if(!_terapeuta || _terapeuta.length === i)
          return res.notFound({err: 'No se ha encontrado ningun terapeuta en este registro'});

        return res.ok(`El terapeuta con id  ${terapeutaId} será eliminado`);
      }).catch(err => res.serverError(err));
  },

  findAll: function (req, res) {

    terapeuta.find().then(_terapeuta =>{
      if (!_terapeuta || _terapeuta.length === 0){
        throw new Error('Terapeuta no encontrado')
      }

      return res.ok(_terapeuta);
    })
      .catch(err => res.serverError(err));
  },

  findOne: function (req, res) {
    let terapeutaId = req.params.id;
    if(!terapeutaId) return res.badRequest({err:'Falta el campo de id'})
    Terapeuta.findOne({id: terapeutaId})
      .then(_terapeuta =>{
      if (!_terapeuta )
        return res.notFound({err: 'Terapeuta no enontrado'})
      return res.ok(_terapeuta);
    })
      .catch(err => res.serverError(err));
  },

  login: function (req, res) {

    console.log(req.body);
    let prueba = req.body;

    let usuario = prueba.nombreUsuario;
    let contrasena = prueba.contrasena;

    console.log(usuario, contrasena);

    if(!usuario || !contrasena){
      return res.badRequest({err:'Falta usuario o contraseña'});
    }

    Terapeuta.findOne({nombreUsuario: usuario}).then(_terapeuta =>{

      if (!_terapeuta){
        return res.badRequest({err:'Terapeuta no encontrado'});}
      else if(_terapeuta.contrasena !== contrasena){
        return res.badRequest({err:'Contraseña Incorrecta'});
      }else if (_terapeuta.contrasena === contrasena){
        return res.json(_terapeuta);

      }

    })

  },

  reset:function (req, res) {

    let prueba = req.body;
    console.log(prueba)
    let usuario = prueba.nombreUsuario;

    if(!usuario ){
      return res.badRequest({err:'Falta usuario'});
    }

    Terapeuta.findOne({nombreUsuario: usuario}).then(_terapeuta =>{

      if (!_terapeuta){
        return res.badRequest({err:'Terapeuta no encontrado'});
      }
      else {
        return res.json( _terapeuta)
      }
    })
  }
};

