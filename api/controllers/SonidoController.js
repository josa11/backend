/**
 * SonidoController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

  upload :function  (req, res) {

    var sonido=  req.param('sonido');
    var tipo = req.param('tipo');
    var identificador= req.param('identificador');

    //console.log(identificador);


    if(req.method === 'GET')

      return res.json({'status':'GET no permitido'});

    // Call to /upload via GET is error
    var upFile = req.file('upFile');


    if(tipo==='Asociación Imagen-Sonido') {
      var path = '../../assets/Sonidos/asociacion/'+ identificador ;
    }else {
      var path = '../../assets/Sonidos/secuencia/'+identificador;
    }
    var mkdirp = require('mkdirp');

    mkdirp(path, function(err) {
      if (err) {
        console.log("folder not created: ", err);
        return res.serverError(err);
      }
      console.log("folder created or existed!")

      upFile.upload({maxBytes: 1000000000, dirname: path,  adapter: require('skipper-disk') },function onUploadComplete (err, files) {
        if (err) {
          console.log(" file not uploaded. Error: "+err);
          return res.serverError(err);
        }
        else {
          //for (let u in files) {

          var filePath = files[0].fd;
          var fileSize = files[0].size;
          var fileType = files[0].type;
          var fileName = files[0].filename;


          Sonido.create({
            name: fileName,
            version: 0,
            description: fileType,
            file: fileName,
            fileSize: fileSize,
            fullPath: filePath,
            sonido: sonido,
          }, function Done(err) {
            return res.ok();

          });
        }
        //}
      });
    });


  },



};

